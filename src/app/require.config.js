/* eslint no-unused-vars: "off" */
var require = {
    baseUrl: ".",
    paths: {
        text: "lib/requirejs-text/text",
        domReady: "lib/requirejs-domready/domReady",
        
        signals: "lib/js-signals/dist/signals.min",
        crossroads: "lib/crossroads/dist/crossroads.min",
        hasher: "lib/hasher/dist/js/hasher.min",
        
        knockout: "lib/knockout/dist/knockout",
        "knockout-projections": "lib/knockout-projections/dist/knockout-projections.min",
        "knockout-dragdrop": "lib/knockout-dragdrop/lib/knockout.dragdrop",
        
        bliss: "lib/blissfuljs/bliss.min",        
    },
    shim: {
        "knockout-projections": ["knockout"],
        bliss: {
            exports: "$"
        },
        "socket.io": {
            exports: "io"
        },
    },
    waitSeconds : 60,
};
