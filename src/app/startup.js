/* eslint-env browser */

require(["knockout", "crossroads", "hasher", "app/app", "app/translate", "knockout-dragdrop", "app/bindings", "domReady!"],
function(ko, crossroads, hasher, AppViewModel, _) {

    ko.options.deferUpdates = true;
    
    // request notifications permission
    window.Notification.requestPermission();
    
    // register components
    
    var components = [
        "notifications",
        "friend-list",
        "messages"
    ];
    
    ko.utils.arrayForEach(components, function(component) {
        ko.components.register(component, { require: "components/" + component + "/" + component });
    });
    
    // routing
    
    var routes = [
        {url: "", needsLogin: true, params: { page: "client-page" }},
        {url: "create", needsLogin: true, params: { page: "custom-game-page" }},
        {url: "/login", needsLogin: false, params: { page: "login-page" }},
        {url: "/game/{id}", needsLogin: true, params: { page: "game-page"}, rules: { id: /^[0-9]+$/ }}
    ];
    
    var authFailureRoute = "login";
    var authSuccessRoute = "";
    
    // this observable connects crossroads/hasher to knockout
    var currentRoute = ko.observable(null);
    
    ko.utils.arrayForEach(routes, function(route) {
        ko.components.register(route.params.page, { require: "components/" + route.params.page + "/" + route.params.page });
        
        var crossroadsRoute = crossroads.addRoute(route.url, function(requestParams) {
            if (route.needsLogin && (typeof window.appViewModel === "undefined" || !window.appViewModel.gameClient.socketClient.isConnected)) {
                hasher.setHash(authFailureRoute);
            } else {
                currentRoute(ko.utils.extend(requestParams, route.params));
            }
        });
        
        if (route.rules) {
            crossroadsRoute.rules = route.rules;
        }
    });
    
    function hashChanged(newHash, oldHash) {
        crossroads.parse(newHash);
    }

    crossroads.normalizeFn = crossroads.NORM_AS_OBJECT;
    hasher.changed.add(hashChanged);
    hasher.initialized.add(hashChanged);
    hasher.init();
    
    // fire up knockout
    window.appViewModel = new AppViewModel(currentRoute, authSuccessRoute, authFailureRoute);
    ko.applyBindings(window.appViewModel);
    
});
