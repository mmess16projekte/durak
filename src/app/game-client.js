/* eslint-env browser */
/* global define */
define(["knockout", "hasher", "app/socket-client", "models/game", "models/friend", "models/notification"], function(ko, hasher, SocketClient, Game, Friend, Notification) {
    
    function GameClient(authSuccessRoute, authFailureRoute) {        
        this.socketClient = new SocketClient();
        
        this.connected = ko.observable(false);
        
        this.userId = ko.observable(0);
        this.username = ko.observable("");
        this.email = ko.observable("");
        
        this.notifications = ko.observableArray([]);
        this.friends = ko.observableArray([]);
        this.games = ko.observableArray([]);
        
        this.sortedFriends = ko.pureComputed(function() {
            var friends = this.friends();
            
            friends.sort(function(a, b) {
                if (a.loggedIn() && !b.loggedIn()) {
                    return -1;
                } else if (b.loggedIn() && !a.loggedIn()) {
                    return 1;
                } else if (a.username() < b.username()) {
                    return -1;
                } else if (b.username() < a.username()) {
                    return 1;
                }
                
                return 0;
            });
            
            return friends;
        }, this);
        
        this.onlineFriends = ko.pureComputed(function() {
            var friends = this.sortedFriends(),
                onlineFriends = [];
            
            for (var i = 0; i < friends.length; ++i) {
                if (friends[i].loggedIn()) {
                    onlineFriends.push(friends[i]);
                }
            }
            
            return onlineFriends;
        }, this);
        
        this.sortedGames = ko.pureComputed(function() {
            var games = this.games();
            
            games.sort(function(a, b) {
                if (a.updatedAt() > b.updatedAt()) {
                    return -1;
                } else if (b.updatedAt() > a.updatedAt()) {
                    return 1;
                }
                
                return 0;
            });
            
            return games;
        }, this);
        
        this.onConnected = this.onConnected.bind(this);
        this.onDisconnected = this.onDisconnected.bind(this);
        this.onGoto = this.onGoto.bind(this);
        this.onNotification = this.onNotification.bind(this);
        this.onFriendUpdate = this.onFriendUpdate.bind(this);
        this.onMessage = this.onMessage.bind(this);
        this.onGameUpdate = this.onGameUpdate.bind(this);
        
        this.socketClient.connected.add(this.onConnected);
        this.socketClient.disconnected.add(this.onDisconnected);
        this.socketClient.goto.add(this.onGoto);
        this.socketClient.notification.recieve.add(this.onNotification);
        this.socketClient.friendship.update.add(this.onFriendUpdate);
        this.socketClient.game.update.add(this.onGameUpdate);
        this.socketClient.message.recieve.add(this.onMessage);
        
        // copy all sending functions from socketClient to GameClient
        for (var namespaceKey in this.socketClient.socketRoutes) {
            if (this.socketClient.socketRoutes.hasOwnProperty(namespaceKey)) {
                var namespace = this.socketClient.socketRoutes[namespaceKey];
                
                this[namespaceKey] = {};
                
                for (var actionKey in namespace) {
                    if (namespace.hasOwnProperty(actionKey) && namespace[actionKey] === true) {
                        this[namespaceKey][actionKey] = this.socketClient[namespaceKey][actionKey];
                    }
                }
            }
        }
    }
    
    GameClient.prototype.onConnected = function(client) {
        var self = this;
        
        self.connected(true);
        self.userId(client.user.id);
        self.username(client.user.username);
        self.email(client.user.email);
        
        if (client.user.notifications) {
            self.notifications(client.user.notifications.map(function(notification) {
                return new Notification(self, notification);
            }));
        }
        
        if (client.user.friends) {
            self.friends(client.user.friends.map(function(friend) {
                return new Friend(friend);
            }));
        }
        
        if (client.user.games) {
            self.games(client.user.games.map(function(game) {
                return new Game(game);
            }));
        }
    };
    
    GameClient.prototype.onDisconnected = function(client) {
        this.connected(false);
        this.userId(0);
        this.username("");
        this.email("");
        
        this.notifications([]);
        this.friends([]);
        this.games([]);
    };
    
    GameClient.prototype.onGoto = function(client, route) {
        hasher.setHash(route);
    };
    
    GameClient.prototype.onNotification = function(client, notification) {
        this.notifications.push(new Notification(this, notification));
    };
    
    function updateCreate(T, list, obj) {
        var listUnwrapped = ko.unwrap(list);
        
        for (var i = 0; i < listUnwrapped.length; ++i) {
            if (listUnwrapped[i].id === obj.id) {
                if (typeof T.prototype.update === "function") {
                    listUnwrapped[i].update(obj);
                } else {
                    list.splice(i, 1, new T(obj));
                }
                
                return;
            }
        }
        
        list.push(new T(obj));
    }
    
    GameClient.prototype.onFriendUpdate = function(client, friend) {
        updateCreate(Friend, this.friends, friend);
    };
    
    GameClient.prototype.onMessage = function(client, friendId, message) {
        var friends = this.friends();
        
        for (var i = 0; i < friends.length; ++i) {
            if (friends[i].id === friendId) {
                friends[i].messages.push({
                    timestamp: new Date(),
                    message: message,
                    from: friends[i].username()
                });
                break;
            }
        }
    };
    
    GameClient.prototype.onGameUpdate = function(client, game) {
        updateCreate(Game, this.games, game);
    };
    
    GameClient.prototype.login = function(username, password) {
        return this.socketClient.login(username, password);
    };
    
    GameClient.prototype.create = function(username, password, email) {
        return this.socketClient.create(username, password, email);
    };
    
    GameClient.prototype.acceptNotification = function(notification) {
        if (notification.data.accept) {
            this.socketClient.socket.emit(notification.data.accept, notification.data.data);
        }
        
        this.notifications.remove(notification);
    };
    
    GameClient.prototype.denyNotification = function(notification) {
        if (notification.data.deny) {
            this.socketClient.socket.emit(notification.data.deny, notification.data.data);
        }
        
        this.notifications.remove(notification);
    };
    
    return GameClient;
});
