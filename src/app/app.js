/* eslint-env browser */
/* global define */
define(["knockout", "hasher", "app/game-client"], function(ko, hasher, GameClient) {
    
    var MOTDs = [
        "Spieler vereinigt euch!",
        "Das Spiel des kleinen Mannes!",
        "Nicht, das der Chef das sieht!",
        "Liberte, Egalite, Fraterite!",
        "Sitzen gegen das Wachstum!",
        "Wer hat uns verraten?",
        "Komm herunter, log dich ein!",
        "Fuer die Horde!",
        "Die Holzwand muss weg!",
        "Ohne Mietvertrag!",
        "Been there, done that.",
        "Made in Germany!",
        "Vegan & Glutenfrei*!",
        "... :)",
        "Fills you with determination!",
        "Kill the animals!",
        "Wir haben Internet!",
        "Does not end in .io!",
        "Mark fehlt!",
        "Win until you lose!",
        "Web-scale!",
        "Throw it on the ground!",
        "Bis der Endboss kommt!",
        "Hurra, die Welt geht unter!",
    ];
    
    function AppViewModel(route, authSuccessRoute, authFailureRoute) {
        var self = this;
        
        self.route = route;
        
        self.authSuccessRoute = authSuccessRoute;
        self.authFailureRoute = authFailureRoute;
        
        self.gameClient = new GameClient();
        
        self.connected = self.gameClient.connected;
        
        self.motd = MOTDs[Math.floor(Math.random() * MOTDs.length)];

        self.asides = [
            {
                component: "messages",
                icon: "fa-envelope",
                visible: ko.observable(false),
                highlighted: ko.observable(false)
            },
            {
                component: "friend-list",
                icon: "fa-users",
                visible: ko.observable(false),
                highlighted: ko.observable(false)
            },
            {
                component: "notifications",
                icon: "fa-comment",
                visible: ko.observable(false),
                highlighted: ko.observable(false)
            }
        ];
        
        self.gameClient.socketClient.connected.add(function() {
            self.setHash(self.authSuccessRoute);
        });
        
        self.gameClient.socketClient.disconnected.add(function() {
            self.setHash(self.authFailureRoute);
        });
        
        self.toggleAside = self.toggleAside.bind(self);
    }
    
    AppViewModel.prototype.setHash = function(hash) {
        hasher.setHash(hash);
    };
    
    AppViewModel.prototype.toggleAside = function(panel) {
        for (var i = 0; i < this.asides.length; ++i) {
            if (this.asides[i] === panel) {
                this.asides[i].visible(!this.asides[i].visible());
                
                if (this.asides[i].visible()) {
                    this.asides[i].highlighted(false);
                }
            } else {
                this.asides[i].visible(false);
            }
        }
    };
 
    AppViewModel.prototype.openGame = function(id) {
        hasher.setHash("game/" + id);
    };
    
    AppViewModel.prototype.redirectLogin = function() {
        if (this.gameClient.socketClient.isConnected) {
            hasher.setHash(this.authSuccessRoute);
        } else {
            hasher.setHash(this.authFailureRoute);
        }
    };
    
    return AppViewModel;
});
