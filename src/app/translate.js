/* eslint-env browser */
/* global define */
define(["knockout"], function(ko) {
    
    function translate(value) {
        if (typeof value === "string") {
            return value;
        }
        
        var copied = value.slice();
        
        var str = "" + copied.shift();
        
        while (str.indexOf("%s") >= 0 && copied.length > 0) {
            str = str.replace(/%s/, "" + copied.shift());
        }
        
        return str;
    }
    
    ko.bindingHandlers.translatedText = {
        preprocess : function(val) {
            return val || true;
        },
        
        init: function(element, valueAccessor) {
            
            ko.computed(function() {
                var value = ko.unwrap(valueAccessor());
                
                if (typeof value === "boolean") {
                    element.innerHTML = translate(element.innerHTML);
                } else {
                    ko.utils.setTextContent(element, translate(value));
                }
            }, null, { disposeWhenNodeIsRemoved: element });
            
            return { controlDescendantBindings: true };
        },
        
        update: function() {}
    };
    
    ko.bindingHandlers.translatedPlaceholder = {
        preprocess : function(val) {
            return val || true;
        },
        
        init: function(element, valueAccessor) {
            ko.computed(function() {
                var value = ko.unwrap(valueAccessor());
                
                if (typeof value === "boolean") {               
                    element.setAttribute("placeholder", translate(element.getAttribute("placeholder")));
                } else {
                    element.setAttribute("placeholder", translate(value));
                }
            }, null, { disposeWhenNodeIsRemoved: element });
            
            return { controlDescendantBindings: true };
        },
        
        update: function() {}
    };
    
    return translate;
});

