/* eslint-env browser */
/* global define */
define(["bliss", "knockout", "text!components/messages/messages.html"], function(Bliss, ko, template) {

    function Messages(params) {
        var self = this;
        
        self.appViewModel = params.app;
        self.visible = params.visible;
        self.hasNew = params.highlighted;
        
        self.friends = self.appViewModel.gameClient.onlineFriends;
        
        self.selectedFriend = ko.observable(null);
        
        self.messagesOfSelectedFriend = ko.pureComputed(function() {
            var selectedFriend = self.selectedFriend();
            
            if (selectedFriend) {
                return selectedFriend.messages();
            }
            
            return [];
        });
        
        ko.computed(function() {
            var messages = self.messagesOfSelectedFriend();
            
            if (!self.visible.peek() && messages.length > 0) {
                self.hasNew(true);
                
                var message = messages[messages.length - 1];
                
                // eslint-disable-next-line no-unused-vars
                var n = new window.Notification(message.from, {
                    body: message.message,
                    icon: "/res/icon_192x192.png"
                });
            }
        });
        
        self.messageToSend = ko.observable("");
    }
    
    Messages.prototype.scrollDown = function(element, index, data) {
        var messageNodes = Bliss.$("li.message", element[0].parentNode);
        
        messageNodes[messageNodes.length - 1].scrollIntoView();
    };
    
    Messages.prototype.sendMessage = function() {
        var selectedFriend = this.selectedFriend(),
            messageBody = this.messageToSend();
        
        this.appViewModel.gameClient.message.send(selectedFriend.id, messageBody);
        
        selectedFriend.messages.push({
            timestamp: new Date(),
            message: messageBody,
            from: this.appViewModel.gameClient.username()
        });
        
        this.messageToSend("");
    };
    
    return { viewModel: Messages, template: template };
});
