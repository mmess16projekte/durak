var passportSocketIo = require("passport.socketio"),
    io = require("../init/socketio");

module.exports = {
    sendFriendshipUpdates: function(user, userDataOverride) {
        var userData = user.toJSON();
        
        if (typeof userDataOverride !== "undefined") {
            for (var key in userDataOverride) {
                if (userDataOverride.hasOwnProperty(key)) {
                    userData[key] = userDataOverride[key];
                }
            }
        }
        
        return user.related("friends").fetch()
            .then(function(friends) {
                var friendIds = [];
            
                friends.forEach(function(friend) {
                    friendIds.push(friend.get("id"));
                });
            
                passportSocketIo.filterSocketsByUser(io, function(socketUser) {
                    return friendIds.indexOf(socketUser.id) >= 0;
                }).forEach(function(socket) {
                    socket.emit("friendship/update", userData);
                });
            });
    }
};
