var bookshelf = require("../init/bookshelf");

require("./user");

var Notification = bookshelf.Model.extend({
    tableName: "notifications",
    hasTimestamps: true,
    
    user: function() {
        return this.belongsTo("User", "user_id");
    },
    
    format: function(attrs) {
        if (typeof attrs !== "undefined") {
            if (typeof attrs.message !== "undefined" && attrs.message !== null) {
                attrs.message = JSON.stringify(attrs.message);
            }
            if (typeof attrs.data !== "undefined" && attrs.data !== null) {
                attrs.data = JSON.stringify(attrs.data);
            }
        }
        
        return attrs;
    },
    
    parse: function(attrs) {
        if (typeof attrs !== "undefined") {
            if (typeof attrs.message !== "undefined" && attrs.message !== null) {
                attrs.message = JSON.parse(attrs.message);
            }
            if (typeof attrs.data !== "undefined" && attrs.data !== null) {
                attrs.data = JSON.stringify(attrs.data);
            }
        }
        
        return attrs;
    }
});

module.exports = bookshelf.model("Notification", Notification);
