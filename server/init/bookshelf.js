var config = require("../config.json"),
    knex = require("knex")({ client: "mysql", connection: config.db }),
    bookshelf = require("bookshelf")(knex);

bookshelf.plugin("registry");

module.exports = bookshelf;
