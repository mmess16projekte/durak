var passport = require("passport"),
    LocalStrategy = require("passport-local").Strategy,
    User = require("../models/user");

var userFetchOptions = {
    required: true,
    withRelated: [
        "friends",
        "notifications",
        "games",
        "games.players"
    ],
};

var strategy = new LocalStrategy(function(username, password, done) {
    User.where("username", username).fetch(userFetchOptions)
        .then(function(model) {
            if (model.validatePassword(password)) {
                done(null, model);
            } else {
                done(null, false, { message: "Ungültiger Benutzername oder Passwort!" });
            }
        })
        .catch(function(err) {
            console.log(err);
            done(err, false, { message: "Ungültiger Benutzername oder Passwort!" });
        });
});

passport.use("local", strategy);

passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(function(user, done) {
    User.where("id", user).fetch({ required: true })
        .then(function(model) {
            done(null, model);
        })
        .catch(function(err) {
            done(err);
        });
});


module.exports = passport;
