var passportSocketIo = require("passport.socketio"),
    io = require("../init/socketio"),
    User = require("../models/user"),
    Friendship = require("../models/friendship"),
    Notification = require("../helpers/notification"),
    FriendshipHelper = require("../helpers/friendship");

module.exports = {
    socketSend: function(username) {
        var self = this;
                
        User.where("username", username).fetch({ require: true, columns: ["id"] })
            .then(function(model) {
                if (model.get("id") === self.user.id) {
                    throw new Error("cannot friend yourself!");
                }
            
                // we don't have to check for existence here, the database will throw an integrity constraint error
                return Friendship.forge({
                    "user1_id": self.user.id,
                    "user2_id": model.id
                }).save();
            })
            .then(function(friendship) {
                Notification.send(friendship.get("user2_id"),
                                  "Freundschaftsanfrage",
                                  ["%s möchte dich als Freund hinzufügen!",
                                   self.user.get("username")],
                                  self.user.id,
                                  "friendship/accept",
                                  "friendship/deny");
            
                Notification.send(self.user.id,
                    "Anfrage gesendet",
                    ["Deine Freundschaftsanfrage an %s wurde erfolgreich gesendet.", username]);
            })
            .catch(function() {
                Notification.send(self.user.id, "Nicht gefunden", ["Es konnte kein Spieler mit dem Namen '%s' gefunden werden!", username]);
            });
    },
    
    socketAccept: function(friendId) {
        var self = this;
        
        User.where("id", friendId).fetch({ require: true })
            .then(function(friend) {
                Friendship.forge({
                    "user1_id": self.user.get("id"),
                    "user2_id": friend.get("id")
                }).save()
                    .then(function() {
                        // update both sides.
                        self.emit("friendship/update", friend.toJSON());

                        passportSocketIo.filterSocketsByUser(io, function(socketUser) {
                            return socketUser.get("id") === friend.get("id");
                        }).forEach(function(userSocket) {
                            userSocket.emit("friendship/update", self.user.toJSON());
                        });
                    });
            });
    },
    
    socketDeny: function(friendId) {
        Friendship.forge({"user1_id": friendId, "user2_id": this.user.id}).fetch()
            .then(function(model) {
                if (model) {
                    model.destroy();
                }
            });
    },
    
    socketConnect: function(socket) {
        //eslint-disable-next-line camelcase
        FriendshipHelper.sendFriendshipUpdates(socket.user, { logged_in: true, last_login: new Date() });
    },
    
    socketDisconnect: function(socket) {
        //eslint-disable-next-line camelcase
        FriendshipHelper.sendFriendshipUpdates(socket.user, { logged_in: false });
    }
};
