var passportSocketIo = require("passport.socketio"),
    io = require("../init/socketio");

module.exports = {

    socketSend: function(friendId, message) {
        var self = this;
    
        passportSocketIo.filterSocketsByUser(io, function(user) {
            return user.id === friendId;
        }).forEach(function(socket) {
            socket.emit("message/recieve", self.user.id, message);
        });
    }
    
};
